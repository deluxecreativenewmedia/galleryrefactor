public class Main {

    public static void main(String[] args) {

        for(int i=1;i<21;i++) {
            String image = "\"Galleries/IC_storyboards_SC85_99";
            if(i<10) {
                image = "RA2_Gal_TheNightclub_0"+i;
            }
            else if(i<100) {
                image = "RA2_Gal_TheNightclub_"+i;
            }
            else {
                image = "FOREST_Gal_Storyboard_Cabin_"+i;
            }
            System.out.println("{\n" +
                    "\"src\": \"Galleries/"+image+".jpg\",\n" +
                    "\"caption\":\"\"\n" +
                    "},");
        }
    }
}